__author__ = 'Gagan'

import os
import sys
import time


if os.path.exists('packs.zip'):
    sys.path.insert(0, 'packs.zip')

try:
    import pyspark
except:
    import findspark
    findspark.init()
    import pyspark


from pyspark.sql import SparkSession
from pyspark.ml.feature import OneHotEncoderEstimator, StringIndexer, VectorAssembler
from pyspark.ml import Pipeline
from pyspark.ml.classification import DecisionTreeClassifier
from pyspark.ml.classification import RandomForestClassifier
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.sql.types import IntegerType


if __name__ == "__main__":
    ts = str(int(time.time()))

    print('=-' * 20)
    print(len(sys.argv))
    print(sys.argv)
    print('=-' * 20)

    spark = SparkSession \
        .builder \
        .appName("pipeline-name" + ts) \
        .getOrCreate()

    HDFS_BASE_PATH = "gs://dataprc-ust-poc/data/data.csv"

    data_df = spark.read.load(HDFS_BASE_PATH, format="csv", inferSchema="true", header="true", mode="DROPMALFORMED")
    data_df.printSchema()

    categoricalColumns = ['EMPLOYER_NAME', 'JOB_TITLE', 'FULL_TIME_POSITION', 'WORKSITE', 'YEAR']
    numericColumns = ['PREVAILING_WAGE']

    pipeline_stages = []
    df = data_df.withColumn("PREVAILING_WAGE", data_df["PREVAILING_WAGE"].cast(IntegerType()))
    cols = df.columns
    df.printSchema()


    for categoricalCol in categoricalColumns:
        stringIndexer = StringIndexer(inputCol = categoricalCol, outputCol = categoricalCol + 'Index')
        encoder = OneHotEncoderEstimator(inputCols=[stringIndexer.getOutputCol()], outputCols=[categoricalCol + "classVec"])
        pipeline_stages += [stringIndexer, encoder]

    label_stringIdx = StringIndexer(inputCol = 'CASE_STATUS', outputCol = 'label')
    pipeline_stages += [label_stringIdx]

    assemblerInputs = [c + "classVec" for c in categoricalColumns] + numericColumns
    assembler = VectorAssembler(inputCols=assemblerInputs, outputCol="features")
    assembler.setParams(handleInvalid="skip")
    pipeline_stages += [assembler]

    pipeline = Pipeline(stages = pipeline_stages)
    pipelineModel = pipeline.fit(df)
    df = pipelineModel.transform(df)
    selectedCols = ['label', 'features'] + cols
    df = df.select(selectedCols)
    df.printSchema()

    train, test = df.randomSplit([0.8, 0.2], seed = 2018)
    print("n\nTraining Dataset Count: " + str(train.count()))
    print("Test Dataset Count: " + str(test.count()))


    rf = RandomForestClassifier(featuresCol = 'features', labelCol = 'label')
    rfModel = rf.fit(train)
    predictions = rfModel.transform(test)
    predictions.printSchema()

    predictions.rdd.saveAsTextFile("gs://dataprc-ust-poc/data/output/predictions/")

    evaluator = MulticlassClassificationEvaluator()
    print("\n\nAccuracy: " + str(evaluator.evaluate(predictions, {evaluator.metricName: "accuracy"})))

    spark.stop()